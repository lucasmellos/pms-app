var mysql      = require('mysql');
var config = require('./config.js');

module.exports = {
    insert: insert,
    connection: connection
}

function connection(){
    var connection = mysql.createConnection(config.dbconfig);
       
      connection.connect(function(err) {
          if (err) {
            console.error('error connecting: ' + err.stack);
            return;
          }
         
          console.log('connected as id ' + connection.threadId);
        });
    return connection;
}

function insert(connection, sensor, value){
    var sql = "INSERT INTO data(`sensor_id`, `voltage`) VALUES ('"+sensor+"', '"+value+"')";
    console.log(sql);
    connection.query(sql, function (error, results, fields) {
        if (error) throw error;
        console.log('INSERT: OK');
    });
}