var measure = require('./measure.js');
var express = require('express');
var app = express();

var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port =  80;

app.use(express.static(__dirname + '/public'));

server.listen(port, function () {
  console.log('Server listening at port %d', port);
  
});

measure.setMeasurement(true); // start measurement

io.on('connection', function (socket) {
    socket.on('teste', function(data){
      socket.emit('teste',data);
    });
    
  socket.on('new message', function(data){
        console.log('new connection' + data);
  
        if(data){
            measure.setMeasurement(true);
        }else{
            measure.setMeasurement(false);
        }
    });
});
  
app.get('/:command', function (req, res) {
	console.log(req.params.command);
	if(req.params.command){
		measure.setMeasurement(true);
	}else{
        measure.setMeasurement(false);
	}
});
// lab.getInfo(function(err, temp) {
//     console.log('getInfo',err,temp);
// });

// lab.getVoltage(1, function(err, temp) {
//     console.log('getVoltage',err,temp);
// });


// lab.setMeasurement(function(err, temp) {
//     console.log('getInfo',err,temp);
// });


