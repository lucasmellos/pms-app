var modbus = require('./modbus_queue.js');

module.exports = {
    getInfo: getInfo,
    getVoltage: getVoltage,
};

function getVoltage(value, callback) {
    console.log("value: "+ value);
    modbus.command('getV'+value, null, function(err, data) {
        console.log(err,data);
        if (err) {
            callback({
                message: err.message,
                strack: err.strack
            });
        } else {
            var data = data.data; 
            callback(null, data);
        }
    });
}
 
function getInfo(callback) {
    
    modbus.command('getInfo',null, function(err, data) {
        // console.log(err,data);
        if (err) {
            callback({
                message: err.message,
                strack: err.strack
            });
        } else {
            var data = data.data;
            var str = String.fromCharCode((data[0] & 0xFF), (data[0] >> 8), (data[1] & 0xFF), (data[1] >> 8), 0, (data[2] & 0xFF), (data[2] >> 8), 0);
            callback(null, str);
        }
    });
}

