var db = require('./db.js');
var lab = require('./lab.js');
var config = require('./config.js');

module.exports = {
    setMeasurement: setMeasurement,
}

var connection = db.connection();
var measure = true;

function setMeasurement() {
    if (measure == true){
        lab.getVoltage(0, function(err, temp) {
            console.log('getV1: ',err,temp);
            db.insert(connection, 1, temp);    
        });
        lab.getVoltage(1,function(err, temp) {
            console.log('getV2: ',err,temp);
            db.insert(connection, 2, temp);    
        }); 
        lab.getVoltage(2,function(err, temp) {
            console.log('getV3: ',err,temp);
            db.insert(connection, 3, temp);    
        }); 
        lab.getVoltage(3,function(err, temp) {
            console.log('getV4: ',err,temp);
            db.insert(connection, 4, temp);    
        }); 
    } 
    
    setTimeout(setMeasurement, config.interval * 60000);

}   
