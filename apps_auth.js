// Setup basic express server
var lab = require('./lab.js');
var express = require('express');
var app = express();
var fs = require('fs')
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var cors = require('cors');
var Auth = require('./auth.js');
var bodyParser = require('body-parser') 
var jsonexport = require('jsonexport');

app.use(bodyParser.json())

app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static(__dirname + '/public/'))

app.get('/', cors(), function(req, res, next) {
    var data = fs.readFileSync(__dirname + '/public/index.html', 'utf8');
    res.send(data);
});

app.post('/export', cors(), function(req, res, next) {
   var input = [];
   for(var obj in req.body){
       input.push(req.body[obj]);
   }
   jsonexport(input,function(err, csv){
        if(err) 
            return console.log(err);
       res.set({"Content-Disposition":"attachment; filename=\"dados.csv\""});
       res.send(csv);
    });
});

var secret = '7dac9fdcf87d2800f8c90c22a675469q';
var port = 80;
var ssi_address = 'localhost:8080';
var lab_id = 1;


io.on('connection', function(socket) {

    var auth = new Auth(ssi_address, secret, lab_id)
    var interval;

    socket.on('new connection', function(data) {

        console.log('new connection ', data, new Date());

        if (typeof(data.pass) === 'undefined') {
            socket.emit('err', {
                code: 402,
                message: 'Missing authentication token.'
            });
            console.log('erro 402');
            return;
        }

        var ev = auth.Authorize(data.pass);

        ev.on("not authorized", function() {
            socket.emit('err', {
                code: 403,
                message: 'Permission denied. Note: Resource is using external scheduling system.'
            });
            console.log('not authorized');
            return;
        })

        ev.on("authorized", function() {
            lab.getScale(function(err2, data) {
                if (err2) socket.emit('err', err2);
                else socket.emit('scale', data);
            });

        })

    });



    socket.on('scale', function(data) {
        console.log(data, new Date());
        if (!auth.isAuthorized()) {
            socket.emit('err', {
                code: 403,
                message: 'Permission denied. Note: Resource is using external scheduling system.'
            });
            console.log('erro 403');
            return;
        }

        lab.setScale(data.scale, function(err, writedata) {
            if (err) socket.emit('err', err);
            console.log(writedata);
            lab.getScale(function(err2, scale) {
                if (err2) socket.emit('err', err2);
                else socket.emit('scale', scale);
            });
        });

    });



    socket.on('start', function(data) {

        if (!auth.isAuthorized()) {
            socket.emit('err', {
                code: 403,
                message: 'Permission denied. Note: Resource is using external scheduling system.'
            });
            console.log('erro 403');
            return;
        }
        
        function sendTemp() {
            lab.getTemp(function(err, data) {

                if (err) {
                    console.log('eer', err);
                } else {
                    console.log('data', data);
                    socket.emit('temperature', data);
                }
            });
        }

        lab.setSetpoint(data.setpoint, function(err) {
            if (err) 
                socket.emit('err', err);
            else{
                lab.start(function(err2, data) {
                    if (err2) socket.emit('err', err2);
                    else {
                        sendTemp();
                        interval = setInterval(sendTemp, (data.interval > 1 ? data.interval : 1) * 1000);
                    }
                });
            }
        });

    });

    socket.on('stop', function(data) {

        if (!auth.isAuthorized()) {
            socket.emit('err', {
                code: 403,
                message: 'Permission denied. Note: Resource is using external scheduling system.'
            });
            console.log('erro 403');
            return;
        }

        if (data.stopTempStreaming) {
            clearInterval(interval);
        }

        lab.stop(function(err2, data) {
            if (err2) socket.emit('err', err2);
            else {
                socket.emit('scale', data);
            }
        });

    });


    socket.on('disconnect', function() {
        if (!auth.isAuthorized()) {
            socket.emit('err', {
                code: 403,
                message: 'Permission denied. Note: Resource is using external scheduling system.'
            });
            console.log('erro 403');
            return;
        }

        clearInterval(interval);

        lab.stop(function(err2, data) {
            if (err2) socket.emit('err', err2);
            else {
                console.log(data);
                console.log('turning lab off', new Date());
            }
        });

        console.log('disconnected', new Date());

    });


});


server.listen(port, function() {
    console.log('Server listening at port %d', port);
    console.log(new Date());
});